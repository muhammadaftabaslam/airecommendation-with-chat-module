/*
 * Stats Saving and Serving over socket. 
 * Author: M. Aftab Aslam
 * copyrigths: liveadmins.com LLC
 */
var events = require('events'),
util = require('util'), url = require('url');
var statsEvent = new events.EventEmitter();
var crypto = require('crypto');
var async = require('async'); 
colors = require('colors');
var _ = require('underscore');
var fs = require('fs');
var path = require('path');

var sk_stats = require('../lib/sk_stats'),
	bt_stats = require('../lib/bt_stats');
	tm_stats = require('../lib/tm_stats');
	fileio = require('../lib/fileio');
	visitor_stats = require('../lib/visitor');
	redisio = require('../lib/redis');
	web_stats = require('../lib/website');
	socketio = require('../lib/socketServer');
	recommend = require('../lib/recommender');
	mssql = require('../lib/ms_sql');
	webstats = require('../lib/stats_lib');
	
function ajaxPost(req,res){
	
		//console.log("post received"+req.body.data)
		console.log("**********************************Post recieved**********************************************".inverse);	
		//var data = visitor_stats.ParseVisitorData(req.body.data);	
		try
		{
		res.end("ok",function(){
			visitor_stats.ParseVisitorData(req.body.data,function(data)
			{
		
			/*
				Three types of posts
				1. Searched Keywords 
				2. Behavoir Tracking
				3. Time Tracking
			*/
				if(data.visitorid != undefined)
				{
					console.log("Post recieved from WEB".green);
					var webID = data.websiteid;
					web_stats.RecordWebsiteWiseStats(data);
					//storing visitor ID a hash. 
					redisio.SetHashFields("visitorIds",data.visitorid,1,function(){});
					switch(data.postType){
						
							case 'BT':
										bt_stats.BehavoirTracking(data); 
										//webstats.makeJSON(webID,function(data){
										//	socketio.socketEmit(webID,data);
										//});
										break;	
							case 'SK':						
										sk_stats.SearchKeywordsTracking(data); 
										
										//webstats.makeJSON(webID,function(data){
										//	socketio.socketEmit(webID,data);
										//});
										break;
							case 'TM':	
										tm_stats.TimeTracking(data); 
										
										//if(data.time<17 && data.active){
										//	webstats.makeJSON(webID,function(data){
										//		socketio.socketEmit(webID,data);
										//	});
										//}
										break;
							default:
							//res.end();
					}
				}
				else
				{
					console.log('Visitorid not received , malfunctioning of cookie');
				}
			});
		});
		
	} 
	catch (e) 
	{
		console.error("Parsing error in stats.js:", e); 
	}
	
}	 
exports.searchPost = function(req,res){
    var queryString = url.parse(req.url,true).query;
    if(_.isEmpty(queryString)){
        ajaxPost(req,res);
    }
    else{
        //console.log('GET Request with query string  for IE = ',queryString);
		req.body.data = queryString.data;
		console.log("IE Request found***************" );
		ajaxPost(req,res);
    } 
}