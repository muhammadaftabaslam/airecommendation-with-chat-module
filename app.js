
/**
 * Behavoir Tracking
 * Liveadmins
 * copyright 2014.
 * Author: M. Aftab Aslam
 * Technologies Used
 * Node Server
 * Socket Server
 * Redis Server 
 */
 
// Allow cross domain function
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    else {
      next();
    }
};
var express = require('express');
var stats = require('./routes/stats');
var http = require('http');
var path = require('path');
var fs = require('fs');
colors = require('colors');
var redis_store = require('redis');
var async = require('async');
var lib = require('./lib/stats_lib');
redis = redis_store.createClient();
// Redis client for deleting session of a visitor having no TM for last 5 mints 
/*subscriberClient = redis_store.createClient();
subscriberClient.psubscribe("__keyevent@0__:expired");
subscriberClient.on("pmessage", function (pattern, channel, expiredKey) {
	var visitorid = expiredKey;
    console.log("key ["+  visitorid +"] has expired");
	redis.exists("Ref:"+visitorid,function(err,status)
	{
		//mssql.NewRequest(function(){
		//	console.log("Logging SQL function");
		//});
		if(status)
		{
			redisio.SessionClose(visitorid,function(){
			console.log("Implicitly Session expired visitor id :", visitorid);
			});
		}
		else
		{
			console.log("Hash not exist : ", visitorid);
		}
	});
});*/

// Redis on error function
redis.on('error', function(err){
	console.log(("REDIS ERROR: " + redis.host + " : " + redis.port + " - " + err).red );
});
var app = express();
app.use(allowCrossDomain);
app.set('port', process.env.PORT || 8099);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.cookieParser());
app.use(express.session({cookie: { path: '/', httpOnly: true, maxAge: null }, secret:'liveadmins'}));
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}
//app.get('/cron',lib.runCron);
app.get('/web_stat/:id',lib.generateJSON);
app.post('/ajaxpost',stats.searchPost);
app.get('/ajaxpost',stats.searchPost);
app.get('/gifFileStats',stats.searchPost);
app.get('/',function(req,res){
	res.send("This page may be used for checking Dubai server stats i guess");
});



// Node server listen on port 9000
server = http.createServer(app);
server.listen(app.get('port'), function(){
  console.log(('Express server listening on port ' + app.get('port')).green);  
  setInterval(lib.runningCron ,86400000);
});
var fileio = require('./lib/fileio');
var stats_lib = require('./lib/stats_lib');
var socketio = require('./lib/socketServer');
io = require('socket.io').listen(server);
io.sockets.on('connection', function (socket) { // Function for socket connection with .Net WG chat software

	/*fileio.VisitorLogs("Sockets","Socket_Connections","Socket request received :"+socket.id+ "Date : "+new Date(),function(){
	
		//console.log("Complete Socket",socket);
	});*/
    console.log("Socket Connection Open");
	socket.on('VisitorID', function (visitorid) {
	/*fileio.VisitorLogs("Sockets","Socket_Connections","Visitor ID : "+visitorid+" Request for data : "+socket.id+ " Date : "+new Date(),function(){
		console.log("Get data request",socket.id);
	});*/
		stats_lib.hashExists(visitorid,function(hashExist){
			console.log("hashExist",hashExist);
			if(hashExist)
			{
				redisio.SetHashFields("Ref:"+visitorid,"socketid",socket.id,function(){
					console.log("socket id set : ",socket.id );
					stats_lib.DataPushToOperator(visitorid,function(visitorfiledata) {
					/*fileio.VisitorLogs("Sockets","Socket_Connections","Visitor ID : "+visitorid+" Data emitted on socket : "+socket.id+ " Date : "+new Date()+" Data: "+JSON.stringify(visitorfiledata),function(){
						console.log("Get data request",socket.id);
					});*/
						//console.log("Data calculated   ++++++++++: ",visitorfiledata );
						console.log("Request for the data from Operator : ",visitorid );
						socket.emit('VisitorData',visitorfiledata);
					
					});
				});
			}
			else
			{
				/*fileio.VisitorLogs("Sockets","Socket_Connections","Visitor ID : "+visitorid+" No Ref for this visitor on operator request : "+socket.id+ " Date : "+new Date(),function(){
						console.log("@@@@@*** No Ref for this visitor on operator request ***@@@@@",visitorid);
				});*/
				
			}
		});
	});
	socket.on('EndChat', function (visitorid) {
			redis.hdel("Ref:"+visitorid, "socketid",function(err,rem){
						console.log("end chat call recieved".red);
					});
			
		});
	
	socket.on('disconnect', function () {
		//io.sockets.emit('user disconnected');
		/*fileio.VisitorLogs("Sockets","Socket_Connections","Socket Disconnect event received",function(){
						console.log("Socket Disconnect event received");
				});*/
	});
	
	
});
