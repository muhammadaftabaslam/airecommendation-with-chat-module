
/**
 * Filter Tracking
 * Search Tracking
 * Time Tracking
 * Liveadmins
 * copyright 2014.
 * Author: Sahal Tariq 
 */
 
var La ={
	//to check whether the cookie exists or not
	cookieExists : function(){
		//console.log("new PAGE");
		if(La.getCookie('tabCookie')==""){
			return false;
		}
		else{
			return true;
		}
	},
	getValuesArrayCheckbox : function(_class) {
        var selected = {};
        $('#search-filters input:checked').each(function() {
            if (selected[$(this).attr('name')] == undefined)
            {
                selected[$(this).attr('name')] = new Array();
            }
            selected[$(this).attr('name')].push($(this).attr('value'));
        });
        $('#search-filters input:checkbox:not(:checked)').each(function() {
            if (selected[$(this).attr('name')] == undefined)
            {
                selected[$(this).attr('name')] = "";
            }

        });
        return selected;
    },
	// checks whether the click was on an anchor or not
	preocessClick:function(e){ 
		var target = e.target || e.srcElement;
		if(target.tagName.toLowerCase() === 'a'){
			return true;
		}
		else{
			return false;
		}
	},
	searchBoxClick : function (e) {
		var clickedElement = (window.event) ? window.event.srcElement : e.target,
			tags = document.getElementsByTagName(clickedElement.tagName);
		for (var i = 0; i < tags.length; ++i) {
			if (tags[i] == clickedElement) {
				if(clickedElement.id == search_id)
				{
					search_box_was_clicked = true;	
				}
				else
				{
					if(search_box_was_clicked && $('#'+search_id).val() != "" && $('.result').text()!="")
					{
						//console.log("1");
						La.search_base_call_for_case_two_and_three("1");
						search_box_was_clicked = false;
					}
					else if(search_box_was_clicked && $('#'+search_id).val() != "" && $('.result').text()=="")
					{
						//console.log('0');
						La.search_base_call_for_case_two_and_three("0");
						search_box_was_clicked = false;
					}
				}
				arrayWithElements.push({
					tag: clickedElement.tagName,
					index: i
				});
			}
		}
	},
	// is called whenever a click is made on the website
	onClick:function(e){
		var target = e.target || e.srcElement;
		La.searchBoxClick(e);
		if(La.preocessClick(e)){
			La.incrTabNum('tabCookie');
		}
		else{
			
			//La.incrTabNum('tabCookie');
			console.log(target.className);
			switch(target.className){
				case 'thm-general_border thm-box_gradient thm-light_text_color refine_search':{
					La.incrTabNum('tabCookie');
					var myCheckBoxValues = {};        
					var cookiestr = JSON.stringify(La.getValuesArrayCheckbox('search-filters'));
					if (myCheckBoxValues != "") {
						var cookieValue = La.getCookie('B_Track');
						if (cookieValue != ""){
							La.setCookies('B_Track',cookiestr);
							La.setCookies('BB_Track',cookiestr);
						}
						else{
							La.setCookies('B_Track',cookiestr);
							La.setCookies('BB_Track',cookiestr);
							
						}
					}else{
						//console.log(" i will never show up");
					}
					break;
				}
				case 'search-veh_img thm-general_border':{
					var texta = $(this).html();
					var n = texta.split("<");
					var actionurl = n[5].substring(9);
					actionurl = actionurl.substring(0, actionurl.length - 3);
					var cookieStr = La.getCookie('BB_Track');
					if (cookieStr != "")
					{
						La.incrTabNum('tabCookie');
						var completeValues = {};
						var resultCookie = La.readCookieJson("WG_VIC_J");
						var resultwi = La.readCookieJson("WG_WEB");
						completeValues = JSON.parse(cookieStr); 
						completeValues["actionurl"] = actionurl;
						var text = actionurl.split("/");
						completeValues["postType"] = "BT";
						completeValues["visitorid"] = resultCookie.V_I;
						completeValues["websiteid"] = resultwi.W_I;
						completeValues["pid"] = text[text.length - 1];
						completeValues["flag"] = 2;
						La.ajaxCall(JSON.stringify(completeValues));
						La.delCookie('BB_Track');
					}
					break;
				}
				case 'result thm-general_border best fl_l mw_t1':{
					console.log('SK');
					var texta = $(this).html();
					var n = texta.split("<");
					var a = n[1].substring(9);
					var actionurl = a;
					actionurl = actionurl.substring(0, actionurl.length - 10);
					var text = a.split("/");
					text.pop();
					var search_result = {};
					var resultCookie = La.readCookieJson("WG_VIC_J");
					var resultwi = La.readCookieJson("WG_WEB");
					search_result["websiteid"] = resultwi.W_I;
					search_result["visitorid"] = resultCookie.V_I;
					search_result["searchedtext"] = $('#s_search').val();
					search_result["postType"] = "SK";
					search_result["flag"] = "2";
					search_result["actionurl"] = WebAddress + '/' + actionurl;
					search_result["websiteurl"] = WebAddress;
					search_result["pid"] = text[text.length - 1];            
					La.ajaxCall(JSON.stringify(search_result));
					break;
				}
			}
		}
	},
	//called before leaving a page
	beforeUnload:function(e){
		La.decrTabNum('tabCookie');
		var sessionValue = La.checkSession(La.getCookie("tabCookie"));
		var search_result = La.leavingPage(sessionValue,false);
		La.ajaxCall(search_result);
	},
	beforeLeaving:function(data){
		var json = JSON.parse(data);			
		json[document.URL]--;
		//console.log("Before leaving UPDATE: "+ JSON.stringify(json));
		return JSON.stringify(json);	
	},
	//used for reading visitor id and website id from cookies
	 readCookieJson:function(name) {
        var returnData = "";
        var nameEQ = name + "=";
        var ca = document.cookie.split(";");
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == " "){
                c = c.substring(1, c.length);
            }
            if (c.indexOf(nameEQ) == 0)
                returnData = c.substring(nameEQ.length, c.length)
        }
        if (returnData != "")
            return eval("(" + returnData + ")");
        else
            return ""
    },
	leavingPage:function(sessionValue,timeflag) {
	// reload always sessionValue = true
	// reload always timeflag = true
		var search_result = {};
		var resultCookie = La.readCookieJson("WG_VIC_J");
		var resultwi = La.readCookieJson("WG_WEB");
		console.log( "La.readCookieJson(WG_VIC_J) = ", resultCookie, "La.readCookieJson(WG_WEB) ", resultwi);
		/*
			check here if the resultCookie is not null or empty or any other .... 
		*/
		
		if(resultCookie AND resultwi COOKIE both exists) 
		then {
		
        search_result["websiteid"] = resultwi.W_I;
        search_result["visitorid"] = resultCookie.V_I;
        search_result["postType"] = "TM"; 
        search_result["actionurl"] = host;
        search_result["websiteurl"] = WebAddress;
		search_result["active"] = timeflag;
		search_result["session"] = sessionValue;
		}
		else
		{
			NO cookie found 
			do some control logic stuff here. 
			by pass the rest of the flow of this file
			and just test and fix this portion. 
			please report asap. 
			regards. 
		}
        var pageClose = new Date();
        var hours = pageClose.getHours() - pageOpen.getHours();
        var minutes = pageClose.getMinutes() - pageOpen.getMinutes();
        var seconds = pageClose.getSeconds() - pageOpen.getSeconds();
        var time = (seconds + (minutes * 60) + (hours * 60 * 60));
		if(time < 2)
		{
			search_result["time"] = 1;
		}
		else
		{
			search_result["time"] = time;
		}
		console.log("JSON sent to ajax: "+ JSON.stringify(search_result));
		return JSON.stringify(search_result);
	},
	//Checks if any tab was open or not
	checkSession:function(data){
		console.log('Number of tabs : '+data);
		if(data>0)
		return true;
		else{
		//console.log("JSON OBJ: "+JSON.stringify(json));
			La.delCookie("tabCookie");
			window.name='';
			return false;
		}
	},
	search_base_call_for_case_two_and_three : function (value)
	{
				var search_result = {};
				var resultCookie = readCookieJson("WG_VIC_J");
				var resultwi = readCookieJson("WG_WEB");
                search_result["websiteid"] = resultwi.W_I;
				search_result["visitorid"] = resultCookie.V_I;
                search_result["searchedtext"] = $('#s_search').val();
				search_result["postType"] = "SK";
                search_result["flag"] = value;
                search_result["websiteurl"] = document.URL;
                search_result["actionurl"] = "";
				search_result["pid"] = ""; 
				//console.log(searchfor);           
				ajaxCall(JSON.stringify(search_result));	
	},
	ajaxCall :function(search_result){	
	console.log("AJAX CALL FOR: "+search_result);
	if(navigator.userAgent.search("MSIE 8.0")> -1 || navigator.userAgent.search("MSIE 7.0")> -1 || navigator.userAgent.search("MSIE 9.0")> -1){
			La.Internet_Explorer(search_result);
		}
	else{
		   $.ajax({
				url: "http://94.200.178.58:3000/ajaxpost",
				type: "Post",
				async: false,
				data: {
					"data": search_result
				},
				success: function(data) {//console.log("success  ", data);				
				},
				error: function(err) {	console.log("unsuccess 2 :"+JSON.stringify(err));
				}
			});
		}
		//console.log("PAGE LEFT");
	},
	Internet_Explorer:function (myCheckBoxValues) {
    
    var timeout = 10000;
	 var url = 'http://94.200.178.58:3001/ajaxpost?id='+myCheckBoxValues;
		if (window.XDomainRequest) {
			xdr = new XDomainRequest();
			if (xdr) {
				xdr.onerror = function () {

					if (typeof console != "undefined" || typeof console.log != "undefined")
						console.log("In Errror");
				};
				xdr.timeout = timeout;
				xdr.open("Get", url);
				//alert(xdr.contentType);
				xdr.send();

			} 
		}
	},	
	delCookie:function(name){
		document.cookie = name + '=\'nothing\'; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
		//console.log("deleting cookie: "+name+':'+La.getCookie(name)); 
	},
	setCookies:function(name, value) {
        document.cookie = name + "=" + escape(value) + "; path=/";
    },
	updateCookies:function(name, value) {
		La.delCookie(name);
        document.cookie = name + "=" + escape(value) + "; path=/";
     
        
    },
	setExpiration:function(cookieLife) {
        var today = new Date();
        var expr = new Date(today.getTime() + cookieLife * 24 * 60 * 60 * 1000);
        return  expr.toGMTString();
    },
	getCookie:function(w) {
		var cName = "";
		var pCOOKIES = new Array();
		pCOOKIES = document.cookie.split('; ');
		for (var bb = 0; bb < pCOOKIES.length; bb++) {
			var NmeVal = new Array();
			NmeVal = pCOOKIES[bb].split('=');
			if (NmeVal[0] == w) {
				cName = unescape(NmeVal[1]);
			}
		}
		return cName;
	},
	refresh:false,
	mouseMover:function(){// is called whenever mouse is moved on the website
		var nowTime = new Date();
		if(La.getCookie('timeCookie')==""){
			/*Remake the urls cookie if it is removed*/
			
			var search_result = La.leavingPage(true,timeflag);
			La.ajaxCall(search_result);
		}
		La.setCookies("timeCookie",nowTime);
		
	},
	//to check whether the visitor is active on the website or not
	timedOut:function(time1){ 
		var nowTime = new Date(time1);
		if(time1!= ""){
			var newTime = new Date();
			var diff = newTime.getTime() - nowTime.getTime(); // diff would be the difference in milliseconds
			if(diff>=120000){ //if difference is greater or equal to 2 mins
			
				//console.log('delete timeCookie, Session timeout');
				return true;
			}
			//console.log('timeCookie remains, Session not timedout');
			return false;
		}
		//console.log('timeCookie does not exist');
		return true;
	},
	//set the name of a window, new tab is detected by the window name
	setWindowName:function(name){
		window.name=name;
	},
	//add one to cookie if a new is opened
	incrTabNum:function(cookieName){
		var value = La.getCookie(cookieName);
		value++;
		//console.log('incr');
		La.updateCookies("tabCookie",value);
	},
	// subtract one from cookie if tab is closed
	decrTabNum:function(cookieName){
		var value = La.getCookie(cookieName);
		value--;
		//console.log('decr');
		La.updateCookies("tabCookie",value);
	},
	//called to send BT post
	found_data:function(flagvalue){
		var cookieString = La.getCookie('B_Track');
        if (cookieString != "")        
        {
			var resultCookie = La.readCookieJson("WG_VIC_J");
			var resultwi = La.readCookieJson("WG_WEB");
            var completeValues = {};
			completeValues = JSON.parse(cookieString);
			//console.log('values : ',completeValues);
            completeValues["visitorid"] = resultCookie.V_I;
			completeValues["websiteurl"] = WebAddress;
			completeValues["websiteid"] = resultwi.W_I;
            completeValues["postType"] = "BT";
            completeValues["actionurl"] = "";
            completeValues["pid"] = "";
            completeValues["flag"] = flagvalue;
			//console.log('values : ',completeValues);
            La.ajaxCall(JSON.stringify(completeValues));
			//setCookie('BB_Track','', -1, '/',WebAddress, 1);
			if(flagvalue == 0)
			{
				La.delCookie("BB_Track");
				//La.setCookie('BB_Track','', -1, '/',WebAddress, 1);
			}
        }
	}
};

var timeflag = true;
var search_box_was_clicked =  false;
var host = window.location.href;
var search_id = 's_search';
var arrayWithElements = new Array();
pageOpen = new Date();
var WebAddress = window.location.host;

(function($){
	console.log('Checking jQuery' , $);	
	var search_result = La.leavingPage(true,timeflag);
	La.ajaxCall(search_result);
	
	if ($(".search_no-results").text().length === 0) {
		La.found_data("1");
		La.delCookie("B_Track");
	}
	else
	{
		La.found_data("0");
		La.delCookie("B_Track");
	}
	
	
	if(La.cookieExists()){
		console.log('cookie exists');
		if(window.name!='LAWin'){ //new Tab
			La.decrTabNum("tabCookie");
			//console.log('new tab');
			La.setWindowName('LAWin');
			La.incrTabNum("tabCookie");
		}
	}
	else{//first visit
		console.log('setting cookie');
		La.setCookies("tabCookie",1);
		La.setWindowName('LAWin');
	}
	
	
	if (!document.addEventListener) {// for IE
		document.attachEvent("onclick", La.onClick);
		document.attachEvent("onmousemove", La.mouseMover);
	}
	else {// for other browsers
		document.addEventListener('click',La.onClick,false);
		document.addEventListener('mousemove', La.mouseMover, false);
	}
	
	
	
	//document.getElementById(search_id).onblur=La.searchBlur;
	
	setInterval(function(){
		if(La.timedOut(La.getCookie('timeCookie'))){ 
			//if user is inactive 
			var search_result = La.leavingPage(false,false);
			if(La.getCookie('timeCookie')!=""){
				La.ajaxCall(search_result);
				La.delCookie('timeCookie');
			}
		}
		else{
			if(!La.cookieExists()){
				La.setCookies("tabCookie",1);
				pageOpen = new Date();
			}
		}
		}, 10000);// check for session timeout every 10 milliseconds
		
	setInterval(function(){ 
		var search_result = La.leavingPage(true,timeflag);
		if(La.timedOut(La.getCookie('timeCookie'))== false){
			La.ajaxCall(search_result);
		}
		},20000);//hearbeat
		
	window.onbeforeunload = La.beforeUnload;
}(jQuery));