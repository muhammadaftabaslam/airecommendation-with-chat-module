var stats_lib = require('./stats_lib'),
	fileio = require('./fileio');
	socketio = require('./socketServer');

exports.BehavoirTracking = function(data){
	console.log("Behavior data : ".green);
	//List for SQL

	stats_lib.ActivityList(data,function()
	{
		console.log(" BT activity save in redis  for SQL".green);
	});
	redis.hset("Ref:"+data.visitorid,"BehaviorTracking",'BT:'+data.visitorid);	// set field in hash 
	if(data["flag"] == 2)
	{
		redis.lpop('BT:'+data.visitorid,function(err,popData)
		{
			//console.log("POP data : ", popData);
			var parser = JSON.parse(popData);
			if(parser["flag"] == 1)
			{
				redis.lpush('BT:'+data.visitorid,JSON.stringify(data));
			}
			else
			{
				redis.lpush('BT:'+data.visitorid,JSON.stringify(parser));
				redis.lpush('BT:'+data.visitorid,JSON.stringify(data));
			}
		
		});
	}
	else
	{
		redis.lpush('BT:'+data.visitorid,JSON.stringify(data)); // Push data in the BT list
	}
	
	var visitordata= {};
	var activities = [];
	activities.push(data);
		visitordata.BT = activities;
		visitordata.Id = data.visitorid;
		visitordata.VT = [];
		visitordata.VLF = [];
		visitordata.SK = []; 

		stats_lib.isOperatorListening(data.visitorid,visitordata,function(){});
}