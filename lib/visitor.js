var redisio = require('./redis');
var stats_lib = require('./stats_lib');
exports.ParseVisitorData = function(data,callback){
	try
	{
		var parser = JSON.parse(data);
		parser['datetime'] = new Date();
		callback(parser);
	} 
	catch (e) 
	{
		console.error("Parsing error:", e); 
	}
}
function FindDataFromTmStats(data){
	try
	{
		var obj = {};
		if(data["productURL"] == true)
		{
			obj.VisitorId = data.visitorid;
			console.log("time is less than 5".red);
			if(data["type"] != "" && data["type"] != undefined)
			{
				console.log("type".red);
				obj["type"] = data["type"]
			}
			if(data["year"] != "" && data["year"] != undefined)
			{
				console.log("year".red);
				obj["year"] = data["year"]
			}
			if(data["make"] != "" && data["make"] != undefined)
			{
				console.log("make".red);
				obj["make"] = data["make"]
			}
			if(data["model"] != "" && data["model"] != undefined)
			{
				console.log("model".red);
				obj["model"] = data["model"]
			}
			return obj;
		}
		else
		{
			console.log("have no data for ".red);
		}
		
	} 
	catch (e) 
	{
		
	}
}


exports.SaveVisitorTMStats = function(data,callback)
{

	/*fileio.VisitorLogs(data.visitorid,data.websiteid,data.visitorid+" : Time Tracking post : URL : " + data.actionurl +" Spent Time :" + data.time + " Page Status : "+ data.active,function(){
	console.log("Visitor Log file");
	});*/
	console.log("Inside TM save stats function");
	
	if (data["active"] === true)// IF PAGE IS ACTIVE
	{
		var obj = {};
	
		obj = FindDataFromTmStats(data);
		var activities = [];
		var visitordata= {};
		var data1 = {};
		data1.URL = data.actionurl;
		data1.TimeSpend = data.time.toString();
		data1.VisitorId = data.visitorid.toString();
		data1.Status = "1";
		activities.push(data1);
		visitordata.VT = activities;
		visitordata.Id = data.visitorid;
		visitordata.SK = []; 
		visitordata.VLF = [];
		if(obj != undefined)
		{
			visitordata.VLF.push(obj);
		}
		
		visitordata.BT = []; 
		stats_lib.isOperatorListening(data.visitorid,visitordata,function(){});
		console.log("Page Active TM");
		if(redis.zscore('TM-Current:'+data.visitorid,data.actionurl) > data.time)// CHECK SCORE OF THE SAME URL IT IT EXIST
		{
			// Do nothing, No need to update data
			callback();
		}
		else
		{
			redis.zadd('TM-Current:'+data.visitorid, data.time, data.actionurl);// PAGE TIME UPDATE
			callback();
		}
	}
	else
	{
		var activities = [];
		var visitordata= {};
		var data1 = {};
		data1.URL = data.actionurl;
		data1.TimeSpend = data.time.toString();
		data1.VisitorId = data.visitorid.toString();
		data1.Status = "0";
		activities.push(data1);
		visitordata.VT = activities;
		visitordata.Id = data.visitorid;
		visitordata.SK = []; 
		visitordata.BT = []; 
		visitordata.VLF = [];
		
		stats_lib.isOperatorListening(data.visitorid,visitordata,function(){});
		console.log("Page Close TM"); // IF PAGE IS INACTIVE
		redis.lpush('TM-List:'+data.visitorid,JSON.stringify(data));
		redis.zincrby("TM-History:"+data.visitorid,data.time,data.actionurl);
		redis.zrem('TM-Current:'+data.visitorid,data.actionurl);
		stats_lib.ActivityList(data,function(){
			console.log("TM activity save in redis for SQL");
			callback();
		});
		
	}
}