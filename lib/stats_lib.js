var fileio = require('./fileio');
var redisio = require('./redis');
var socketio = require('./socketServer');

exports.hashExists = function(visitorid,callback){ // check Hash key exist or not
	redisio.CheckKey("Ref:"+visitorid,function(status){
	callback(status);
	});
}
exports.ActivityList = function(data,callback){ // check Hash key exist or not

	//console.log("Get data for SQL: ",data);
	console.log("inside activity list");
	//if(data.postType != "TM" || (data.postType == "TM" && data.active == "false")){
		console.log("STORING DATA");
		redisio.PushDataToList("Act:"+data.postType,data,function(){
		//console.log("call back from redis on saving SQL stats");
		callback();
		});
	//}
}

exports.DataPushInToSql = function(data,callback){ // check Hash key exist or not
	console.log("length  of data".yellow,data.length);
	for(var i=0;i<data.length;i++)
	{
		var JsonObject = JSON.parse(data[i]);
		switch(JsonObject.postType)
		{
						
			case 'BT':
					mssql.InsertBTPostUsingProc(JsonObject,function(){});
					break;	
			case 'SK':	
					mssql.InsertSKPostUsingProc(JsonObject,function(){});
					break;
			case 'TM':	
					mssql.InsertTMPostUsingProc(JsonObject,function(){});
					break;
			default:
							
		}
	}
	callback();
	
}

exports.ConstructHashForNewVisitor = function(visitorid,callback) // construct hash on first time visit
{
	redisio.SetHashFields("Ref:"+visitorid,"sessionid",1,function(){
	callback();
	});
}

function SessionClose (visitorid,res,callback){ // Session close function
	redisio.SessionClose(visitorid,res,function(){
	callback();
	});
}


exports.checkifSessionClosedTMIsReceived = function(session,callback){ // Check if session closed TM is received
	if(session === true)
	{
		callback(true);
	}
	else
	{
		callback(false);
	}	
}


exports.isOperatorListening = function(visitorid,data,callback){ // Whether operator listening to this visitor
	socketio.CurrentData(visitorid,data,function(){
	callback();
	});
}

exports.DataPushToOperator = function(visitorid,callback){ // function for pushing data to operator
	redisio.DataPushToOperator(visitorid,function(data){
	callback(data);
	});
}

//functions involved with file.io
/*
exports.FileExists = function(visitorid,callback){ // File exist or not
	fileio.FileExists(visitorid,function(status){
	callback(status);
	});
}
exports.ConstructHashFromFile = function(visitorid,callback) //construct hash from file 
{
	fileio.FileData(visitorid,function(filedata){
		var JsonData = JSON.parse(filedata);
		var sessionid = parseInt(JsonData.info.sessionid);
		sessionid++;
		console.log("session id :  ",sessionid);
		redisio.SetHashFields("Ref:"+visitorid,"sessionid",sessionid,function(){
			redisio.SortedSetPush("TM-History:"+visitorid,JsonData.TM,function(){
				redisio.ListPush("BT:"+visitorid,JsonData.BT,function(){
					redisio.ListPush("SK:"+visitorid,JsonData.SK,function(){
						console.log("Constructed Complete Hash from file");
						callback();
					});
				});
			});
		});
	});
}
exports.FileExists = function(visitorid,callback){ // File exist or not
	fileio.FileExists(visitorid,function(status){
	callback(status);
	});
}
*/
/*
* Author: Sahal
*/


function getHeaders(array){
	var str = '';
	var count = 0;
	for (var key in array[0]){
		if (count!=0) str+= ',';
		count++;
		str += key;
	}
	str += '\r\n';
	return str;
}
function ConvertToCSV(objArray) {
	var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
	var str = '';
	str = getHeaders(array);
	for (var i = 0; i < array.length; i++) {
		var line = '';
		var count =0;
		for (var index in array[i]) {
			if(count!=0)line += ',';
			count++;
			if(Array.isArray(array[i][index]))
			{
				var length = array[i][index].length;
				if(length != 1)
				{
					for(var j = 0;j < length; j++){
						if(j > 0) line += ";";
						line += array[i][index][j];
					}
				}
				else
				{
					line += array[i][index];
				}
			}
			else
			{
				line += array[i][index];
			}
		}
		str += line + '\r\n';
	}
	return str;
}

exports.runCron = function(){
	redisio.CheckKey("lastCron",function(exists){
		if(exists){
			redisio.getString("lastCron",function(result){
				//res.write("Last time Cron ran at: "+result+"\n");
				var oldDate = new Date(result);
				var newDate = new Date();
				var difference = (newDate.getTime() - oldDate.getTime())/1000;
				if(difference>86400){ // greater than 24 hours
					runningCron(newDate,function(){
						//res.end("CRON JOB ENDED");
					});
				}
				else{
					res.write("Cron will not run:\n");
					//res.end("24 Hours have not passed since the last cron job\n");
				}
			});
		}else{
			var nowDate = new Date();
			runningCron(nowDate,function(){
				//res.end("CRON JOB ENDED");
			});
			
		}
	});
}

settingCronTime = function(date,callback){
	redisio.setString("lastCron",date,function(){
		callback();
	});
}

exports.runningCron = function(){
	storingInFile(function(){
		redisio.getAllHash("visitorIds",function(result){
			if(result!=null){
				/*settingCronTime(date,function(){
					//res.write("Cron Timer reset to:"+date+"\n");
				});*/
				
				for(var key in result){
					//res.write("Getting keys for visitor ID: "+key+"\n");
					SessionClose(key,res,function(){});
				}
				callback();
			}
			else{
				//res.end("Cron will not run: No keys present");
			}
		});
	});
	
}
storingInFile = function(callback){
	redis.lrange('Act:SK',0,-1,function(error,activities){
		if(error)
		{
			console.log("Error : Redis :  SQL call from redis to SQL file for saving data in SQL on Session Close : ".red,error);
		}
		else
		{
			console.log("SK storing",activities);
			StoreDataInFiles(activities,"SK");
			redis.del('Act:SK');
		}
		});
		redis.lrange('Act:BT',0,-1,function(error,activities){
		if(error)
		{
			console.log("Error : Redis :  SQL call from redis to SQL file for saving data in SQL on Session Close : ".red,error);
		}
		else
		{
			console.log("BT storing",activities);
			StoreDataInFiles(activities,"BT");
			redis.del('Act:BT');
		}
		});
		
		redis.lrange('Act:TM',0,-1,function(error,activities){
		if(error)
		{
			console.log("Error : Redis :  SQL call from redis to SQL file for saving data in SQL on Session Close : ".red,error);
		}
		else
		{
			console.log("TM storing",activities);
			StoreDataInFiles(activities,"TM");
			redis.del('Act:TM');
		}
		});
	callback();	
}

StoreDataInFiles = function(activities,postType){
	//console.log(postType,ConvertToCSV(activities));
	fileio.InsertInFile(ConvertToCSV(activities.map(JSON.parse)),postType);
}

//increments the score of a field in a ZSET.
//maintains a total field in every ZSET to know the sum of scores in a ZSET.
exports.IncrWebsiteStats = function(website,Url,score,callback){
	redisio.IncrementSortedSet(website,Url,score,function(){
		/*redisio.IncrementSortedSet(website,"total",score,function(){
			callback();
		});*/
		callback();
	});
}

//gets the score of any field in a ZSET.
function getWebsiteStat(Key,Field,callback){
	redisio.getZScore(Key,Field,function(result){
		callback(result);
	});
}

//gets the data with the lowest score in a ZSET, within the given range.
function getLeastStats(Key,Range,callback){
	redisio.getLeastZScore(Key,Range-1,function(result){
		callback(result);
	});
}

//gets the data with the Max score in a ZSET, within the given range.
function getMostStats(Key,Range,callback){
	redisio.getMostZScore(Key,Range-1,function(result){
		callback(result);
	});
}

function getMostStatsScore(Key,Range,callback){
	redisio.getMostZScores(Key,Range-1,function(result){
		callback(result);
	});
}

exports.generateJSON=function(req, res){
var webID = req.params.id;
var data = {};
data=makeJSONObj(webID,function(data){
	render(req,res,data,webID);
});
}

exports.makeJSON= function(webID,callback){
	console.log("inside makeJSON");
	makeJSONObj(webID,callback);
	console.log("end makeJSON");
}

function makeJSONObj(webID,callback){
var data = {};
getWebsiteStat(webID+":pageVisits","total",function(result){
	if(result!=null)
	data["total_hits"] = result;
	getWebsiteStat("allWebsitesVisits",webID,function(result){
		if(result!=null)
		data["unique_visitors_count"] = result;
		getMostStats(webID+":keywordUsed",6,function(result){
			if(result.length!=0)
			data["maxKeywords"] = result;
			getMostStatsScore(webID+":keywordUsed",6,function(result){
				if(result.length!=0)
				data["maxKeywordsUsed"] = result;
				getMostStatsScore(webID+":timeSpent",6,function(result){
					if(result.length!=0)
					data["timeSpent"] = result;
					getMostStatsScore(webID+":pageVisits",6,function(result){
						if(result.length!=0)
						data["pageVisits"] = result;
						getMostStatsScore(webID+":productsViewed",6,function(result){
							if(result.length!=0)
							data["productViewed"] = result;
							getMostStatsScore(webID+":filterType",6,function(result){
								if(result.length!=0)
								data["filterType"] = result;
								getMostStatsScore(webID+":filtersUsed",6,function(result){
									if(result.length!=0)
									data["filtersUsed"] = result;
									getLeastStats(webID+":pageVisits",6,function(result){
										if(result.length!=0)
										data["leastPageVisits"] = result;
										callback(data);
									});
								});
							});
						});
					});
				});
			});
		});
	});
});
}


function render(req, res,data,webID){
//console.log("render");
var j =JSON.stringify(data);
//console.log("koooo: ",res.cookie);
//res.cookie('rememberme', "aaa", { maxAge: 900000, httpOnly: false});
res.render('bt', {
	title: "Website Stats for: "+webID,
	data: j,
	webID: webID
});
}




