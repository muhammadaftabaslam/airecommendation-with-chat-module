var stats_lib = require('./stats_lib'),
	fileio = require('./fileio');
	socketio = require('./socketServer');
	
exports.SearchKeywordsTracking = function(data){
	
	console.log('Searched Keywords Data '.green);
	redis.hset("Ref:"+data.visitorid,"SearchTracking",'SK:'+data.visitorid);// set field in hash 
	redis.lpush('SK:'+data.visitorid,JSON.stringify(data)); // Push data in the SK list
	var visitordata= {};
	var activities = [];
	activities.push(data);
		visitordata.SK = activities;
		visitordata.Id = data.visitorid;
		visitordata.VT = [];
		visitordata.VLF = [];
		visitordata.BT = []; 
		stats_lib.isOperatorListening(data.visitorid,visitordata,function(){});
		stats_lib.ActivityList(data,function(){
		console.log(" SK activity save in redis for SQL ".green);
	});
}