var stats_lib = require('./stats_lib');

/*
exports.CheckKeyExists = function(key)//Check key from redis and return its status
{
	redis.exists(key,function(error,status){
		if(error)
		{
			return false;
		}
		else
		{
			return true;
		}
		
	});	
}

exports.getStringValue = function(Key){
	redis.get(Key,function(erroror,result){
		return result;
	});
}

exports.setStringValue = function(Key,value){
	redis.set(Key,value);
}
*/

exports.CheckKey = function(key,callback)//Check key from redis and return its status
{
	redis.exists(key,function(error,status){
		if(error)
		{
			console.log("Error : Redis : Check Key function".red, error);
		}
		else
		{
			callback(status);
		}
		
	});	
}
exports.SetHashFields = function(Key,Field,Value,callback){ // function that Set hash fields 
	//console.log("Hash field setting function : key : "+Key +" field : "+Field+" Value : "+ Value);
	redis.hset(Key,Field,Value,function(){
	//console.log("Field set in hash");
	callback();
	
	});
}
exports.GetHashFields = function(Key,Field,callback){ //function that Get hash field value
	redis.hget(Key,Field,function(error,value){
		if(error)
		{
			console.log("Error : Redis :  Get Hash Field".red, error);
		}
		else
		{
			callback(value);
		}
	
	});
}
exports.SortedSetPush = function(Key,Data,callback){ // function that push data into sorted set get from file
	for(var i=0;i<Data.length;i++)
	{
		redis.zincrby(Key,Data[i+1],Data[i]);
		i++;
	}
	callback();
}
exports.ListPush = function(Key,Data,callback){ // function that push data into lists get from file
	for(var i=0;i<Data.length;i++)
	{
		redis.lpush(Key,Data[i]);
	}
	callback();
}
exports.SetSortedSet = function(Key,Score,Field,callback){
	if(redis.zscore(Key,Field) > Score)
	{
		callback();
	}
	else
	{
		redis.zadd(Key,Score,Field);
		callback();
	}
	
}
exports.PushDataToList = function(Key,data,callback){
//console.log("SQL data for saving ", data);
	redis.lpush(Key,JSON.stringify(data),function(){
	console.log("data saved for SQL");
	callback();
	});
	
}



function ParseData(type,data,visitorid,status) // function for parsing data
{
	var activities = [];
	if(type == "set" && status == "live")
	{
		var u=0;
		while (u<data.length)
		{
			var activities1 = {};
			activities1.URL = data[u];
			u++;
			activities1.TimeSpend = data[u];
			activities1.VisitorId = visitorid;
			activities1.Status = "1";
			activities.push(activities1);
			u++;
		}
		return activities;
	}
	else if(type == "set" && status == "close")
	{
		var u=0;
		while (u<data.length)
		{
			var activities1 = {};
			activities1.URL = data[u];
			u++;
			activities1.TimeSpend = data[u];
			activities1.Status = "0";
			activities1.VisitorId = visitorid;
			activities.push(activities1);
			u++;
		}
		return activities;
	}
	else if(type == "list")
	{
		for(var i=0;i<data.length;i++)
		{
			activities.push(JSON.parse(data[i]));
		}
		return activities;
	}
	else
	{
		return data;
	}


}
function MergingData(array1,array2)
{
	var jsonArray1 = array1.concat(array2);
	return jsonArray1;

}
exports.DataPushToOperator = function(visitorid,callback)// Funtion for making data push to Operator
{
	var multi = redis.multi();
	var visitordata= {};
	redis.exists("Ref:"+visitorid,function(error,status)
	{
		if(error)
		{
			console.log("Error : Redis : Data Push To Operator".red, error);
		}
		else
		{
			if(status)
			{
				multi.lrange('BT:'+visitorid,0,-1);
				multi.lrange('SK:'+visitorid,0,-1);
				multi.zrange('TM-Current:'+visitorid,0,-1,'WITHSCORES');
				multi.zrange('TM-History:'+visitorid,0,-1,'WITHSCORES');
				multi.exec(function(error, results) {	
				visitordata.BT = ParseData("list",results[0],visitorid);
				visitordata.SK = ParseData("list",results[1],visitorid);
				visitordata.VT = MergingData(ParseData("set",results[2],visitorid,"live"),ParseData("set",results[3],visitorid,"close"));
				visitordata.VLF = [];
				
				visitordata.Id = visitorid;
				callback(visitordata);
				});
			}
			else
			{
				visitordata = -1122;
				callback(visitordata);
			}
		}
	});
}
exports.SessionClose = function(visitorid,res,callback) // Session close function
{		var activity = [];
		//res.write("Data saved to SQL for visitor ID: "+visitorid+"\n");
		
		var multi = redis.multi();	
		var Visitor = {};
		redis.zunionstore("UnionSet",2,"TM-History:"+visitorid,'TM-Current:'+visitorid);	
		multi.zrange("UnionSet",0,-1,'WITHSCORES');
		multi.lrange('SK:'+visitorid,0,-1);
		multi.lrange('BT:'+visitorid,0,-1);
		multi.hget('Ref:'+visitorid,"sessionid");
		res.write("Removing keys for visitor ID: "+visitorid+"\n*****************************************************\n");
		multi.exec(function(error, results)
	{	
		if(!error)
		{
			
			Visitor.TM = results[0];
			Visitor.SK = results[1];
			Visitor.BT = results[2];
			Visitor.info = {};
			Visitor.info.sessionid = results[3];
			console.log("all data",Visitor);
			//fs.writeFileSync("./files/"+visitorid,JSON.stringify(Visitor));
			redis.del("visitorIds");
			redis.del('Ref:'+visitorid);
			redis.del('SK:'+visitorid);
			redis.del('BT:'+visitorid);
			redis.del('TM-List:'+visitorid);
			redis.del("UnionSet");
			redis.del('TM-Current:'+visitorid);
			redis.del("TM-History:"+visitorid);
			console.log("ALL KEYS REMOVED");
			callback();
		}
		else
		{
			res.write("Error in recieving keys for visitor ID: "+visitorid);
			console.log("Error : Redis : Session close function error :".red, error);
			callback();
		}
	});
}

exports.SetStringKeyExpire = function(Key,Value,Expire,callback){ // Funtion set expiry on a key
	redis.set(Key,Value,"PX",Expire,function(){
		callback();
	});
}



/*
* Author: Sahal
*/

exports.getAllHash = function(key,callback){
	redis.HGETALL(key,function(error,result){
		callback(result);
	});
}
exports.IncrementSortedSet = function(Key,Field,score,callback){
	redis.zincrby(Key,score,Field,function(){
		callback();
	});
}

exports.getZScore = function(Key,Field,callback){
	redis.zscore(Key,Field,function(error,result){
		callback(result);
	});
}

exports.getLeastZScore = function(Key,Range,callback){
	redis.zrange(Key,0,Range,"withscores",function(error,result){
		callback(result);
	});
}

exports.getMostZScore = function(Key,Range,callback){
	redis.zrevrange(Key,0,Range,function(erroror,result){
		callback(result);
	});
}

exports.getMostZScores = function(Key,Range,callback){
	redis.zrevrange(Key,0,Range,"withscores",function(erroror,result){
		callback(result);
	});
}

exports.getString = function(Key,callback){
	redis.get(Key,function(erroror,result){
		callback(result);
	});
}

exports.setString = function(Key,value,callback){
	redis.set(Key,value,function(){
		callback();
	});
}