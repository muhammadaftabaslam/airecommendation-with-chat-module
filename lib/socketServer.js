var redisio = require('./redis');
var stats_lib = require('./stats_lib');


exports.SocketConnection = function(visitorid,callback) // function if socket exist for a visitor and is still alive the push data
{
	redisio.GetHashFields("Ref:"+visitorid,"socketid",function(socketid)
	{
		console.log("socketid",socketid);
		if(socketid !== "")
		{
			if(io.sockets.sockets[socketid] == undefined)
			{
				console.log("Data cannot emitted".blue);
				redis.hdel("Ref:"+visitorid, "socketid",function(err,rem){
					callback();
				});
			}
			else
			{
				DataCalculate(visitorid,function(visitorfiledata)
				{
					console.log("Emit data on socket".rainbow);
					io.sockets.sockets[socketid].emit('VisitorData',visitorfiledata);
					callback();
				});
			}
		}
		else
		{
			console.log("No socket for "+visitorid+" visitor SocketServer  ".blue);
			callback();
		}
	});
}
function DataCalculate(visitorid,callback) // Just make data in format for sending to operator
{
	stats_lib.DataPushToOperator(visitorid,function(data)
	{
		console.log("data emitted SocketServer");
		callback(data);
	});	
}
/*Sahal*/
exports.socketEmit=function (webID,postData){
	//console.log("inside socketEmit");
	//console.log("post type: ",post);	
	io.sockets.emit('sendTo:'+webID,postData);
}










exports.CurrentData = function(visitorid,data,callback) // function if socket exist for a visitor and is still alive the push data
{
	redisio.GetHashFields("Ref:"+visitorid,"socketid",function(socketid)
	{
		console.log("socketid",socketid);
		if(socketid !== "")
		{
			if(io.sockets.sockets[socketid] == undefined)
			{
				console.log(" CurrentData Data cannot emitted".red);
				redis.hdel("Ref:"+visitorid, "socketid",function(err,rem){
					callback();
				});
				
			}
			else
			{
					console.log("CurrentData Emit data on socket".red,data);
					io.sockets.sockets[socketid].emit('VisitorData',data);
					callback();
			}
		}
		else
		{
			console.log("CurrentData No socket for "+visitorid+" visitor SocketServer  ".red);
			callback();
		}
	});
}















